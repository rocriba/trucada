package cat.dam.rocriba.trucada;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    final int MY_PERMISSIONS_REQUEST_SMS = 1;
    final int MY_PERMISSIONS_REQUEST_CALL = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Initzalitze Buttons
        final Button btn_1 = (Button) findViewById(R.id.btn_1);
        final Button btn_2 = (Button) findViewById(R.id.btn_2);
        final Button btn_3 = (Button) findViewById(R.id.btn_3);
        final Button btn_4 = (Button) findViewById(R.id.btn_4);
        final Button btn_5 = (Button) findViewById(R.id.btn_5);
        final Button btn_6 = (Button) findViewById(R.id.btn_6);
        final Button btn_7 = (Button) findViewById(R.id.btn_7);
        final Button btn_8 = (Button) findViewById(R.id.btn_8);
        final Button btn_9 = (Button) findViewById(R.id.btn_9);
        final Button btn_10 = (Button) findViewById(R.id.btn_10);
        final Button btn_11 = (Button) findViewById(R.id.btn_11);
        final Button btn_12 = (Button) findViewById(R.id.btn_12);
        TextView tv_principal = (TextView) findViewById(R.id.tv_1);
        EditText et_principal = (EditText) findViewById(R.id.et_1);
        tv_principal.setText("");

        //For every Button, make the action, en aquest cas afegeix el numero corresponent a al textview

        btn_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String text = tv_principal.getText().toString();
                tv_principal.setText(text + "1");

            }
        });

        btn_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String text = tv_principal.getText().toString();
                tv_principal.setText(text + "2");

            }
        });

        btn_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String text = tv_principal.getText().toString();
                tv_principal.setText(text + "3");

            }
        });

        btn_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String text = tv_principal.getText().toString();
                tv_principal.setText(text + "4");

            }
        });

        btn_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String text = tv_principal.getText().toString();
                tv_principal.setText(text + "5");

            }
        });

        btn_6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String text = tv_principal.getText().toString();
                tv_principal.setText(text + "6");

            }
        });

        btn_7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String text = tv_principal.getText().toString();
                tv_principal.setText(text + "7");

            }
        });

        btn_8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String text = tv_principal.getText().toString();
                tv_principal.setText(text + "8");

            }
        });

        btn_9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String text = tv_principal.getText().toString();
                tv_principal.setText(text + "9");

            }
        });

        /*
        Primer de tot comprova que tinguem el permís per fer trucades,
        seguidament fa les comprovacions del numero i de el text, en cas de estar bé
        fa la acció, sinó mostra el missatge d'error en el text View.
         */
        btn_10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Demanem el permís

                if (ContextCompat.checkSelfPermission(MainActivity.this,
                        Manifest.permission.SEND_SMS)
                        != PackageManager.PERMISSION_GRANTED) {
                    //En cas de que no el tenim el demanem
                    ActivityCompat.requestPermissions(MainActivity.this,
                            new String[]{Manifest.permission.SEND_SMS},
                            MY_PERMISSIONS_REQUEST_SMS);
                    //Intentem enviar el missatge

                    String missatge = et_principal.getText().toString();
                    String num_telefon = tv_principal.getText().toString();


                    //Tracament del Telefon i EditText
                    boolean nomes_num = true;

                    if(num_telefon.matches(".*\\d.*")){

                        nomes_num= true;
                    } else{

                        nomes_num=false;
                    }

                    if (num_telefon.length() <=8 || missatge == "" ||nomes_num == false ){
                        tv_principal.setText("Dades Incorrectes");
                    } else {
                        if (!TextUtils.isEmpty(missatge)){
                            Intent smsIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:" + num_telefon));
                            smsIntent.putExtra("sms_body", missatge);
                            startActivity(smsIntent);
                        }
                    }

                } else {
                    //El tenim, fem l'sms
                    String missatge = et_principal.getText().toString();
                    String num_telefon = tv_principal.getText().toString();
                    boolean nomes_num = false;

                    if(num_telefon.matches(".*\\d.*")){
                        nomes_num= true;
                    } else{
                        nomes_num=false;
                    }


                    if (num_telefon.length() <=8 || missatge == ""  || nomes_num == false){
                        tv_principal.setText("Dades incorrectes");
                    } else {
                        if (!TextUtils.isEmpty(missatge)){
                            Intent smsIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:" + num_telefon));
                            smsIntent.putExtra("sms_body", missatge);
                            startActivity(smsIntent);
                        }
                    }

                }

            }
        });

        /*
        Comprova si tenim el permís de trucada, sinó el demana,
        seguidament fa l'intent de trucar al mòbil.
         */
        btn_11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Demanem permis, si es que no el tenim
                if (ContextCompat.checkSelfPermission(MainActivity.this,
                        Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED){
                    ActivityCompat.requestPermissions(MainActivity.this,
                            new String[]{Manifest.permission.CALL_PHONE},
                            MY_PERMISSIONS_REQUEST_CALL);

                    //Un cop tenim el permis,fem la trucada

                    String num_telefon = tv_principal.getText().toString();
                    if (!TextUtils.isEmpty(num_telefon)){
                        String dial = "tel:" + num_telefon;
                        startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(dial)));
                    }
                } else {
                    //Ja tenim el permís
                    //Fem la trucada

                    String num_telefon = tv_principal.getText().toString();
                    if (!TextUtils.isEmpty(num_telefon)){
                        String dial = "tel:" + num_telefon;
                        startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(dial)));
                    }
                }
            }
        });


        //Borra la úlltim numero utiltizant un substring
        btn_12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String text = tv_principal.getText().toString();
                int llargada = text.length() - 1;
                String textmenys = text.substring(0, llargada);

                tv_principal.setText(textmenys);

            }
        });


    }}
